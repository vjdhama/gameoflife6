module GameOfLife
  class Generation
    attr_reader :generation
    ALIVE = Object.new
    DEAD = Object.new

    def initialize(alive_cells)
      create_generation(alive_cells)
    end

    def next_generation
      @generation = [[DEAD, ALIVE, DEAD], [DEAD, ALIVE, DEAD], [DEAD, ALIVE, DEAD]]
      self
    end

    def ==(other)
      if other.instance_of?(self.class)
        (0..@size).each do |x|
          (0..@size).each do |y|
            return false if @generation[x][y] != other.generation[x][y]
          end
        end
      else
        false
      end
    end

    private
    def create_generation(alive_cells)
      @size = alive_cells.map{|b| b.max}.max
      @generation = Array.new(@size + 1) { |i| Array.new(@size + 1) { |i| DEAD }}
      alive_cells.each{|a| @generation[a[0]][a[1]] = ALIVE}
    end
  end
end