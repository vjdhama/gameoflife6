require 'spec_helper'

module GameOfLife
  describe Generation do
    it "will give new generation when given alive cells" do
      alive_cells_1 = [[1,0],[1,1],[1,2]]
      generation_1 = Generation.new(alive_cells_1)
      alive_cells_2 = [[0,1],[1,1],[2,1]]
      generation_2 = Generation.new(alive_cells_2)
      expect(generation_1.next_generation).to eq(generation_2)
    end
  end
end